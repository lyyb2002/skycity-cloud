package cn.skycity.common.service.interceptor;

import cn.hutool.core.util.StrUtil;
import cn.skycity.common.constants.SecurityConstants;
import cn.skycity.common.utils.UserUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpRequestHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String username = request.getHeader(SecurityConstants.HEADER_OAUTH_USER);
        if(StrUtil.isNotEmpty(username)) {
            UserUtils.setUserName(username);
        }
        return true;
    }
}
