package cn.skycity.admin.api;

import cn.skycity.admin.pojo.dto.RegisteredClientDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "skycity-user", contextId = "oauth-client")
//@FeignClient(value = "skycity-user")
public interface OAuthClientFeignClient {

    @GetMapping("/api/oauth-clients/{clientId}")
    RegisteredClientDTO getOAuthClientByClientId(@PathVariable String clientId);

    @GetMapping("/api/oauth-clients/id/{id}")
    RegisteredClientDTO getOAuthClientById(@PathVariable String id);
}
