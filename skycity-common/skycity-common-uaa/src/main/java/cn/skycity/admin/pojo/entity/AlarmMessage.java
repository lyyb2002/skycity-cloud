package cn.skycity.admin.pojo.entity;

import lombok.Data;

/**
 * @author daiyb2
 * @Title: AlarmMessage
 * @Description:
 * @date 2021/11/6 9:26
 **/
@Data
public class AlarmMessage {
    private String scopeId;
    private String name;
    private String id0;
    private String id1;
    private String alarmMessage;
    private long startTime;
}
