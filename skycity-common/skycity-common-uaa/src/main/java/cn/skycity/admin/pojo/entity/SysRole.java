package cn.skycity.admin.pojo.entity;

import cn.skycity.common.base.BaseEntity;
import lombok.Data;

@Data
public class SysRole extends BaseEntity {

    private String name;

//    @ApiModelProperty("角色编码")
    private String code;

    private String status;

    private String memo;


}
