package cn.skycity.admin.pojo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author haoxr
 * @date 2020-11-06
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouteVO {

    private Long id;
    private Long parentId;

    private String path;

    private String component;

    private String redirect;

    private String name;

    private String link;

    private Integer menuType;
    private Integer sort;
    private String perm;
    private Integer status;
    private boolean show;

    private Meta meta;

    @Data
    public static class Meta {
        private String title;
        private String icon;
        @JsonProperty("isAffix")
        private boolean affix;
        private boolean visible;
        @JsonProperty("isKeepAlive")
        private boolean keepAlive;
        @JsonProperty("isLink")
        private boolean link;
        @JsonProperty("isBigScreen")
        private boolean bigScreen;

        private String component;

        public Meta(String title,String icon){
            this.title = title;
            this.icon = icon;
        }
    }
    private List<RouteVO> children;
}
