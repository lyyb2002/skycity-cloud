package cn.skycity.admin.pojo.entity;

import cn.skycity.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class SysDept extends BaseEntity {
    private String name;

    private Long parentId;

    private Integer sort;

    private Integer status;

    private String memo;
}
