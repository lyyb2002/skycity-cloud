package cn.skycity.admin.pojo.entity;

import cn.skycity.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class SysDict extends BaseEntity {
    private String code;

    private String name;

    private Integer status;

    private  String  remark;

}
