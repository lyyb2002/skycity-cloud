package cn.skycity.admin.pojo.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Map;
import java.util.Set;

@Data
public class RegisteredClientDTO {
    private Long id;
    private String clientId;
    //客户端生效时间
    private Instant clientIdIssuedAt;
    private String clientSecret;
    //秘钥失效时间
    private Instant clientSecretExpiresAt;
    private String clientName;
    //客户端允许的认证方式
    private Set<String> clientAuthenticationMethods;
    //授权方式
    private Set<String> authorizationGrantTypes;
    private Set<String> redirectUris;
    private Set<String> scopes;
    private Map<String,Object> clientSettings;
    private Map<String,Object> tokenSettings;
}
