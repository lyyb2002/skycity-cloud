package cn.skycity.admin.pojo.entity;

import cn.skycity.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2024/3/24
 * @Version 1.0
 */
@Data
public class SysConfig extends BaseEntity{
    private String configName;
    private String configValue;
    private Integer status;
    private String remark;
}
