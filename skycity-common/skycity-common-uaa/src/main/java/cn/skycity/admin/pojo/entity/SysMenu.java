package cn.skycity.admin.pojo.entity;

import cn.skycity.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author haoxr
 * @date 2020-11-06
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@TableName("sys_menu")
public class SysMenu extends BaseEntity {

    private Long parentId;

    //1：菜单；2：按钮
    private Integer menuType;
    private String perm;
    private String icon;
    //路由路径
    private String path;

    private String component;

    private Integer sort;

    //是否固定
    private Integer affix;
    private Integer status;
    private Integer bigScreen;

    private String link;
    private String title;

    private String redirect;
}
