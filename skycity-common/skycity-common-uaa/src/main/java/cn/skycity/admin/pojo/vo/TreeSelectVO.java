package cn.skycity.admin.pojo.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 树形下拉视图对象
 */
@Data
@Builder
public class TreeSelectVO {
    private Long id;

    private String label;

    private String icon;

    private Integer sort;

    private Integer status;

    private String perm;

    private String component;

    private LocalDateTime createTime;

    private String path;

    private Long parentId;
    private String title;
    private int menuType;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<TreeSelectVO> children;

}
