package cn.skycity.admin.pojo.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserVO {

    private Long id;

    private String username;
    private String nickname;
    private String mobile;
    private String email;

    private String avatar;

    private List<String> roles;

    private List<String> perms;

}
