package cn.skycity.admin.pojo.dto;

import cn.skycity.admin.pojo.entity.SysRole;
import lombok.Data;

import java.util.List;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2024/4/28
 * @Version 1.0
 */
@Data
public class RoleDTO extends SysRole {

    private List<Long> menuIds;
}
