package cn.skycity.admin.api;

import cn.skycity.admin.pojo.entity.SysUser;
import cn.skycity.common.model.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "skycity-user", contextId = "oauth-client",path = "/api/users")
public interface UserFeignClient {

    @RequestMapping("/username/{username}")
    Result<SysUser> getUserByUsername(@PathVariable String username);

}
