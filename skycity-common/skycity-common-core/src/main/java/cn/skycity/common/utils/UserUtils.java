package cn.skycity.common.utils;

public final class UserUtils {
    private static final ThreadLocal<String> USER_NAME = new ThreadLocal<>();

    public static void setUserName(String username){
        USER_NAME.set(username);
    }

    public static String getUserName(){
        return USER_NAME.get();
    }
}
