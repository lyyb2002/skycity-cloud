package cn.skycity.common.model;

/**
 * @author daiyb2
 **/
public interface IResultCode {

    int getCode();
    String getMsg();
}
