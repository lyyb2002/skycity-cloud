package cn.skycity.common;

/**
 * @Description 业务异常
 * @Author YingBo.Dai
 * @Date 2024/4/4
 * @Version 1.0
 */
public class ServiceException extends RuntimeException{

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
