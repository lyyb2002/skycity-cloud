package cn.skycity.common.constants;

public interface GlobalConstants {
    String DEFAULT_USER_PASSWORD = "123456";
    String ROOT_ROLE_CODE = "ROOT";
    String URL_PERM_ROLES_KEY = "system:perm_roles_rule:url:";
    String BTN_PERM_ROLES_KEY = "system:perm_roles_rule:btn:";

    Integer STATUS_YES = 1;

    Integer STATUS_NO = 0;
    int USER_FORBIDDEN = 2;            //账户被禁用
    int USER_LOCK = 3;              //账户被锁定
    int USER_EXPIRED = 4;       //账户过期
}
