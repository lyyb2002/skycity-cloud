package cn.skycity.common.utils;

import cn.hutool.core.util.StrUtil;
import cn.skycity.common.constants.SecurityConstants;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Objects;

/**
 * 请求工具类
 *
 * @author xianrui
 */
@Slf4j
public class RequestUtils {

    @SneakyThrows
    public static HttpServletRequest getRequest(){
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    @SneakyThrows
    public static String getGrantType() {
        return getRequest().getParameter(SecurityConstants.GRANT_TYPE_KEY);
    }


    /**
     * 获取登录认证的客户端ID
     * 从RequestHeader中的Authorization字段，且经过base64编码
     *
     * @return
     */
    @SneakyThrows
    public static String getOAuth2ClientId() {
        String basic = getRequest().getHeader(SecurityConstants.AUTHORIZATION_KEY);
        if (StrUtil.startWith(basic,SecurityConstants.BASIC_PREFIX)) {
            basic = basic.replace(SecurityConstants.BASIC_PREFIX, StrUtil.EMPTY);
            String plainText = new String(Base64.getDecoder().decode(basic), StandardCharsets.UTF_8);
            return StrUtil.subBefore(plainText,":",true);
        }
        return "";
    }

    /**
     * 解析JWT获取获取认证方式
     *
     * @return
     */
//    @SneakyThrows
//    public static String getAuthenticationMethod() {
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        String refreshToken = request.getParameter(SecurityConstants.REFRESH_TOKEN);
//
//        String payload = StrUtil.toString(JWSObject.parse(refreshToken).getPayload());
//        JSONObject jsonObject = JSONUtil.parseObj(payload);
//
//        String authenticationMethod = jsonObject.getStr(SecurityConstants.AUTHENTICATION_METHOD);
//        if (StrUtil.isBlank(authenticationMethod)) {
//            authenticationMethod = AuthenticationMethodEnum.USERNAME.getValue();
//        }
//        return authenticationMethod;
//    }
}
