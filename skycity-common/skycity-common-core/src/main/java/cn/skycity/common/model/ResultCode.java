package cn.skycity.common.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *
 */
@AllArgsConstructor
@NoArgsConstructor
public enum ResultCode implements IResultCode, Serializable {

    SUCCESS(0, "操作成功"),
    ERROR(1,"系统错误"),

    USER_NOT_EXIST(201, "用户不存在"),
    USER_ACCOUNT_LOCKED(202, "用户账户被冻结"),
    USER_ACCOUNT_INVALID(203, "用户账户已作废"),

    USERNAME_OR_PASSWORD_ERROR(210, "用户名或密码错误"),
    PASSWORD_ENTER_EXCEED_LIMIT(211, "用户输入密码次数超限"),
    CLIENT_AUTHENTICATION_FAILED(212, "客户端认证失败"),
    TOKEN_INVALID_OR_EXPIRED(230, "token无效或已过期"),
    TOKEN_ACCESS_FORBIDDEN(231, "token已被禁止访问"),

    AUTHORIZED_ERROR(300, "访问权限异常"),
    ACCESS_UNAUTHORIZED(301, "访问未授权"),

    SYSTEM_EXECUTION_ERROR(801, "系统执行出错");

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    private int code;

    private String msg;

    @Override
    public String toString() {
        return "{" +
                "\"code\":\"" + code + '\"' +
                ", \"msg\":\"" + msg + '\"' +
                '}';
    }


    public static ResultCode getValue(int code){
        for (ResultCode value : values()) {
            if (value.getCode()==code) {
                return value;
            }
        }
        return SYSTEM_EXECUTION_ERROR; // 默认系统执行错误
    }
}
