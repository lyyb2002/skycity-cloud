package cn.skycity.common.utils;

import cn.hutool.json.JSONUtil;
import cn.skycity.common.model.Result;
import cn.skycity.common.model.ResultCode;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName ResponseUtils
 * @Decription TODO
 * @Author YingBo.Dai
 * @Date 2021/10/27
 * @Project cloud
 * 十月
 * 下午10:01 YingBo 2021
 **/
public class ResponseUtils {
    private ResponseUtils() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 通过流写到前端
     *
     * @param response
     * @param msg          返回信息
     * @param httpStatus   返回状态码
     * @throws IOException
     */
    public static void responseWriter(HttpServletResponse response, String msg, int httpStatus) throws IOException {
        Result result = Result.success(httpStatus, msg);
        responseWrite(response, result);
    }

    /**
     * 通过流写到前端
     * @param response
     * @param obj
     */
    public static void responseSucceed(HttpServletResponse response, Object obj) throws IOException {
        Result result = Result.success(obj);
        responseWrite(response, result);
    }

    /**
     * 通过流写到前端
     * @param response
     * @param msg
     * @throws IOException
     */
    public static void responseFailed(HttpServletResponse response, String msg) throws IOException {
        Result result = Result.failed(msg);
        responseWrite(response, result);
    }

    private static void responseWrite(HttpServletResponse response, Result result) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        try (
                Writer writer = response.getWriter()
        ) {
            writer.write(JSONUtil.toJsonStr(result));
            writer.flush();
        }
    }

    public static Mono<Void> writeErrorInfo(ServerHttpResponse response, ResultCode resultCode) {
        switch (resultCode) {
            case ACCESS_UNAUTHORIZED:
            case TOKEN_INVALID_OR_EXPIRED:
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                break;
            case TOKEN_ACCESS_FORBIDDEN:
                response.setStatusCode(HttpStatus.FORBIDDEN);
                break;
            default:
                response.setStatusCode(HttpStatus.BAD_REQUEST);
                break;
        }
        response.getHeaders().set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        response.getHeaders().set("Access-Control-Allow-Origin", "*");
        response.getHeaders().set("Cache-Control", "no-cache");
        String body = JSONUtil.toJsonStr(Result.failed(resultCode));
        DataBuffer buffer = response.bufferFactory().wrap(body.getBytes(StandardCharsets.UTF_8));
        return response.writeWith(Mono.just(buffer))
                .doOnError(error -> DataBufferUtils.release(buffer));
    }


    public static void writeErrorInfo(HttpServletResponse response, ResultCode resultCode) {
        switch (resultCode) {
            case ACCESS_UNAUTHORIZED:
            case TOKEN_INVALID_OR_EXPIRED:
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                break;
            case TOKEN_ACCESS_FORBIDDEN:
                response.setStatus(HttpStatus.FORBIDDEN.value());
                break;
            default:
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                break;
        }
        response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Cache-Control", "no-cache");
        String body = JSONUtil.toJsonStr(Result.failed(resultCode));
        try {
            response.getWriter().write(body);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
