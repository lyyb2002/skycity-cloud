package cn.skycity.common.utils;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.skycity.common.constants.SecurityConstants;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.List;

/**
 * JWT工具类
 *
 * @author xianrui
 */
@Slf4j
public class JwtUtils {

    @SneakyThrows
    public static JSONObject getJwtPayload() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Enumeration<String> header  = request.getHeaderNames();
        String payload = request.getHeader(SecurityConstants.JWT_PAYLOAD_KEY);
        if (null == payload) {
            throw new RuntimeException("请传入认证头");
        }
        return JSONUtil.parseObj(URLDecoder.decode(payload,StandardCharsets.UTF_8));
    }

    /**
     * 解析JWT获取用户ID
     *
     * @return
     */
    public static Integer getUserId() {
        return getJwtPayload().getInt(SecurityConstants.USER_ID_KEY);
    }

    /**
     * 解析JWT获取获取用户名
     *
     * @return
     */
    public static String getUsername() {
        String username = getJwtPayload().getStr(SecurityConstants.JWT_SUB);
        return username;
    }



    /**
     * JWT获取用户角色列表
     *
     * @return 角色列表
     */
    public static List<String> getRoles() {
        List<String> roles = null;
        JSONObject payload = getJwtPayload();
        if (payload.containsKey(SecurityConstants.JWT_AUTHORITIES_KEY)) {
            roles = payload.getJSONArray(SecurityConstants.JWT_AUTHORITIES_KEY).toList(String.class);
        }
        return roles;
    }
}
