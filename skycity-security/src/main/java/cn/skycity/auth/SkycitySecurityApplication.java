package cn.skycity.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Decription TODO
 * @Author YingBo.Dai
 * @Date 2022/8/26 7:27
 * @Version 1.0
 */
@SpringBootApplication
@EnableFeignClients(basePackages = "cn.skycity.admin.api")
@EnableDiscoveryClient
public class SkycitySecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(SkycitySecurityApplication.class, args);
    }
}




