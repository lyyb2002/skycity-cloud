package cn.skycity.auth.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Data
@Component
@ConfigurationProperties("application")
public class ApplicationProperties {
    private final Security security = new Security();

    @Data
    public static class Security {
        private List<String> excludeUrls = new ArrayList<>();
    }
}
