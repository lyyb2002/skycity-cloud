package cn.skycity.auth.authentication;

import com.alibaba.nacos.common.utils.CollectionUtils;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientCredentialsAuthenticationToken;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @Decription 添加通过密码的授权模式
 *  参考： {@link OAuth2ClientCredentialsAuthenticationToken}
 * @Author YingBo.Dai
 * @Date 2022/10/24 21:53
 * @Version 1.0
 */

public class OAuth2ResourcePasswordAuthenticationToken extends OAuth2AuthorizationGrantAuthenticationToken {
    private final String username;
    private final String password;
    private final Set<String> scopes;
    public OAuth2ResourcePasswordAuthenticationToken(String username,String password,Authentication clientPrincipal, @Nullable Set<String> scopes,
                                                     @Nullable Map<String, Object> additionalParameters) {
        super(AuthorizationGrantType.PASSWORD, clientPrincipal, additionalParameters);
        Assert.notNull(clientPrincipal, "clientPrincipal cannot be null");
        this.username = username;
        this.password = password;
        this.scopes = Collections.unmodifiableSet((CollectionUtils.isNotEmpty(scopes) ? new HashSet<>(scopes) : Collections.emptySet()));
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Set<String> getScopes() {
        return this.scopes;
    }

}
