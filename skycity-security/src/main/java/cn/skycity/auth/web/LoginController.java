package cn.skycity.auth.web;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {
//    @Resource
//    private AuthenticationManager authenticationManager;

    @GetMapping("/")
    public Map<String, Object> index(Principal principal) {
        Map<String, Object> map = new HashMap<>(2);
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
        map.put("currentUser", principal.getName());
//        map.put("authorizedClient", oAuth2AuthorizedClient);
        return map;
    }

}
