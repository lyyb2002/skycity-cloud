package cn.skycity.auth.web;


import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/public-api")
    public String publicApi(HttpServletRequest request) {
        request.getParameterNames().asIterator().forEachRemaining(s->{
            System.out.println(s);
        });
        return "this is a public api";
    }

    @RequestMapping("/private-api")
    public String index(HttpServletRequest request){
        System.out.println("privateApi: " + request);
        System.out.println("privateApi: " + SecurityContextHolder.getContext().getAuthentication());
        return "this is private api";
    }
}
