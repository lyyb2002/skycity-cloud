package cn.lyyb2001.blog.service.impl;

import cn.lyyb2001.blog.entity.ArticleTag;
import cn.lyyb2001.blog.mapper.ArticleTagMapper;
import cn.lyyb2001.blog.service.IArticleTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author YingBo.Dai
 * @since 2022-03-05
 */
@Service
public class ArticleTagServiceImpl extends ServiceImpl<ArticleTagMapper, ArticleTag> implements IArticleTagService {

}
