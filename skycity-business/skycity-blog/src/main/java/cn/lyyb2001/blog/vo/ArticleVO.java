package cn.lyyb2001.blog.vo;

import cn.lyyb2001.common.base.BaseEntity;
import cn.lyyb2001.blog.entity.Tag;
import lombok.Data;

import java.util.List;

@Data
public class ArticleVO extends BaseEntity {
    private Integer id;
    private String articleTitle;
    private String articleContent;
    private String articleCover;
    private String categoryName;
    private Integer categoryId;
    private Integer tagId;
    private List<Tag> tagList;
    private String type;
    private String originalUrl;
    private Integer isTop;
    private Integer status;
    private Integer viewsCount;
    private Integer likeCount;

}
