package cn.lyyb2001.blog.mapper;

import cn.lyyb2001.blog.entity.ArticleTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author YingBo.Dai
 * @since 2022-03-05
 */
@Mapper
public interface ArticleTagMapper extends BaseMapper<ArticleTag> {

}
