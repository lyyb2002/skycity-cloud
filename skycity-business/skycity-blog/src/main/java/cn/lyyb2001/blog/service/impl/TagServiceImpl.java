package cn.lyyb2001.blog.service.impl;

import cn.lyyb2001.blog.entity.Tag;
import cn.lyyb2001.blog.mapper.TagMapper;
import cn.lyyb2001.blog.service.ITagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author daiyb2
 * @Title: TagServiceImpl
 * @Description:
 * @date 2021/11/20 22:54
 **/
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements ITagService {
}
