package cn.lyyb2001.blog.mapper;

import cn.lyyb2001.blog.entity.BlogInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author daiyb2
 * @Title: TagMapper
 * @Description:
 * @date 2021/11/20 22:50
 **/
@Mapper
public interface BlogInfoMapper extends BaseMapper<BlogInfo> {
}
