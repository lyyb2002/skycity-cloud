package cn.lyyb2001.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author daiyb2
 * @Title: SkycityBlogApplication
 * @Description:
 * @date 2021/11/20 22:25
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class SkycityBlogApplication {
    public static void main(String[] args) {
        SpringApplication.run(SkycityBlogApplication.class,args);
    }
}
