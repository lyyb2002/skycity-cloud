package cn.lyyb2001.blog.service;

import cn.lyyb2001.blog.dto.BlogInfoDTO;
import cn.lyyb2001.blog.entity.BlogInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 博客信息服务
 *
 * @author yezhiqiu
 * @date 2021/08/09
 */
public interface BlogInfoService extends IService<BlogInfo> {

    /**
     * 获取首页数据
     *
     * @return 博客首页信息
     */
    BlogInfoDTO getBlogInfo();

    /**
     * 上传访客信息
     */
    void report();

}
