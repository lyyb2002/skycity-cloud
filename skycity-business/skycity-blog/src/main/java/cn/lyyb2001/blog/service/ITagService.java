package cn.lyyb2001.blog.service;

import cn.lyyb2001.blog.entity.Tag;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ITagService extends IService<Tag> {

}
