package cn.lyyb2001.blog.mapper;

import cn.lyyb2001.blog.entity.Article;
import cn.lyyb2001.blog.vo.ArticleVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author daiyb2
 * @Title: ArticleMapper
 * @Description:
 * @date 2021/11/20 22:50
 **/
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

    List<ArticleVO> list(Page<ArticleVO> page,@Param("entity")ArticleVO articleVO);

}
