package cn.lyyb2001.blog.entity;

import cn.lyyb2001.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author daiyb2
 * @Title: Tag
 * @Description:
 * @date 2021/11/21 10:36
 **/
@Data
public class Tag extends BaseEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String tagName;
}
