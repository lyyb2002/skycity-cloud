package cn.lyyb2001.blog.service;

import cn.lyyb2001.blog.entity.Article;
import cn.lyyb2001.blog.vo.ArticleVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

public interface IArticleService extends IService<Article> {

    void saveOrUpdateArticle(ArticleVO articleVO);

    ArticleVO getArticleVO(Integer id);

    IPage<ArticleVO> list(Page<ArticleVO> page, ArticleVO articleVO);

    int like(Integer id);
}
