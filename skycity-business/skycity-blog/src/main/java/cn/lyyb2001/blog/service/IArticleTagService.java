package cn.lyyb2001.blog.service;

import cn.lyyb2001.blog.entity.ArticleTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author YingBo.Dai
 * @since 2022-03-05
 */
public interface IArticleTagService extends IService<ArticleTag> {

}
