package cn.lyyb2001.blog.service.impl;

import cn.lyyb2001.blog.entity.Category;
import cn.lyyb2001.blog.mapper.CategoryMapper;
import cn.lyyb2001.blog.service.ICategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author daiyb2
 * @Title: CategoryServiceImpl
 * @Description:
 * @date 2021/11/20 22:54
 **/
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {
}
