package cn.lyyb2001.blog.entity;

import cn.lyyb2001.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName("tb_article")
public class Article extends BaseEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private Integer categoryId;
    private String articleCover;
    private String articleTitle;
    private String articleContent;
    private Integer type;
    private String originalUrl;
    private Integer isTop;
    private Integer isDelete;
    private Integer status;
    private Integer viewsCount;
    private Integer likeCount;
}
