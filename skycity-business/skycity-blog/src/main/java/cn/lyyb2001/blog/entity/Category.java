package cn.lyyb2001.blog.entity;

import cn.lyyb2001.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author daiyb2
 * @Title: Category
 * @Description:
 * @date 2021/11/21 8:06
 **/
@Data
public class Category extends BaseEntity {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String categoryName;
}
