package cn.lyyb2001.blog.service;

import cn.lyyb2001.blog.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ICategoryService extends IService<Category> {

}
