package cn.lyyb2001.blog.controller;

import cn.hutool.core.util.StrUtil;
import cn.lyyb2001.blog.entity.Article;
import cn.lyyb2001.blog.entity.Category;
import cn.lyyb2001.blog.entity.Tag;
import cn.lyyb2001.blog.service.IArticleService;
import cn.lyyb2001.blog.service.ICategoryService;
import cn.lyyb2001.blog.service.ITagService;
import cn.lyyb2001.blog.vo.ArticleVO;
import cn.lyyb2001.common.model.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.skycity.oss.service.IUploadService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author daiyb2
 * @Title: ArticleController
 * @Description:
 * @date 2021/11/20 22:55
 **/
@RestController
@RequestMapping("/articles")
public class ArticleController {

    @Resource
    private IArticleService articleService;
    @Resource
    private ICategoryService categoryService;
    @Resource
    private ITagService tagService;
    @Resource
    private IUploadService uploadService;

    @GetMapping("/pages")
    public Result<IPage<ArticleVO>> pageList(Page<ArticleVO> page,ArticleVO article) {
        IPage<ArticleVO> result = articleService.list(page, article);
        return Result.success(result);
    }

    @GetMapping("/{id}")
    public Result<ArticleVO> getById(@PathVariable("id")Integer id) {
        return Result.success(articleService.getArticleVO(id));
    }

    @GetMapping("/search")
    public Result<List<Article>> search(Integer current, String keywords) {
        if(StrUtil.isNotBlank(keywords)) {
            Page<Article> result = articleService.page(new Page<>(current, 10), new LambdaQueryWrapper<Article>()
                    .like(StrUtil.isNotBlank(keywords), Article::getArticleTitle, keywords));
            return Result.success(result.getRecords(), result.getTotal());
        }else{
            return Result.success();
        }
    }


    @GetMapping("/condition")
    public Result<Map<String,Object>> condition(@RequestParam(required = false) Integer categoryId,
                   @RequestParam(required = false) Integer tagId,
                   Integer current){
        if(categoryId!=null && categoryId!=0){
            Category category = categoryService.getById(categoryId);
            ArticleVO articleVo = new ArticleVO();
            articleVo.setCategoryId(categoryId);
            IPage<ArticleVO> result = articleService.list(new Page<>(current,10),articleVo);
            Map<String,Object> resultMap = Maps.newHashMap();
            resultMap.put("name",category.getCategoryName());
            resultMap.put("data",result.getRecords());
            return Result.success(resultMap);
        }else {
            Tag tag = tagService.getById(tagId);
            ArticleVO articleVo = new ArticleVO();
            articleVo.setTagId(tagId);
            IPage<ArticleVO> result = articleService.list(new Page<>(current,10),articleVo);
            Map<String,Object> resultMap = Maps.newHashMap();
            resultMap.put("name",tag.getTagName());
            resultMap.put("data",result.getRecords());
            return Result.success(resultMap);
        }
    }

    @DeleteMapping("/{ids}")
    public Result<String> delete(@PathVariable String ids) {
        boolean status = articleService.removeByIds(Arrays.stream(ids.split(",")).collect(Collectors.toList()));
        return Result.judge(status);
    }

    @PostMapping("/top")
    public Result<String> setTop(@RequestBody Article article){
        articleService.updateById(article);
        return Result.success();
    }

    @PostMapping("/images")
    public Result<String> saveArticleImages(MultipartFile file) {
        return Result.success(uploadService.executeUploadStrategy(file,"ARTICLE"));
    }

    /**
     * 新增文章
     * @param article
     * @return
     */
    @PostMapping
    public Result<String> saveOrUpdate(@RequestBody ArticleVO article) {
        articleService.saveOrUpdateArticle(article);
        return Result.success();
    }

    @PostMapping("/{id}/like")
    public Result<Boolean> like(@PathVariable("id")Integer id){
        articleService.like(id);
        return Result.success();
    }

}
