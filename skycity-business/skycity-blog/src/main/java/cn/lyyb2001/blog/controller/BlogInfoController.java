package cn.lyyb2001.blog.controller;


import cn.lyyb2001.blog.dto.BlogInfoDTO;
import cn.lyyb2001.blog.service.BlogInfoService;
import cn.lyyb2001.common.model.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 博客信息控制器
 *
 * @author yezhiqiu
 * @date 2021/07/28
 */
@RestController
public class BlogInfoController {
    @Resource
    private BlogInfoService blogInfoService;

    /**
     * 查看博客信息
     *
     */
    @GetMapping("/")
    public Result<BlogInfoDTO> getBlogHomeInfo() {
        return Result.success(blogInfoService.getBlogInfo());
    }

    /**
     * 上传访客信息
     *
     */
    @PostMapping("/report")
    public Result<?> report() {
        blogInfoService.report();
        return Result.success();
    }

}

