package cn.lyyb2001.blog.dto;

import cn.lyyb2001.blog.entity.BlogInfo;
import lombok.Builder;
import lombok.Data;


/**
 * 博客首页信息
 *
 * @author yezhiqiu
 * @date 2021/08/10
 */
@Data
@Builder
public class BlogInfoDTO {

    /**
     * 文章数量
     */
    private Long articleCount;

    /**
     * 分类数量
     */
    private Long categoryCount;

    /**
     * 标签数量
     */
    private Long tagCount;

    /**
     * 访问量
     */
    private Integer viewsCount;

    /**
     * 网站配置
     */
    private BlogInfo blogInfo;
//
//    /**
//     * 页面列表
//     */
//    private List<PageVO> pageList;

}
