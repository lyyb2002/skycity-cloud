package cn.lyyb2001.blog.service.impl;

import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.useragent.Browser;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import cn.lyyb2001.blog.dto.BlogInfoDTO;
import cn.lyyb2001.blog.entity.Article;
import cn.lyyb2001.blog.entity.BlogInfo;
import cn.lyyb2001.blog.mapper.ArticleMapper;
import cn.lyyb2001.blog.mapper.BlogInfoMapper;
import cn.lyyb2001.blog.mapper.CategoryMapper;
import cn.lyyb2001.blog.mapper.TagMapper;
import cn.lyyb2001.blog.service.BlogInfoService;
import cn.lyyb2001.common.utils.RedisUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Service
public class BlogInfoServiceImpl extends ServiceImpl<BlogInfoMapper, BlogInfo> implements BlogInfoService {
    @Resource
    private ArticleMapper articleMapper;
    @Resource
    private CategoryMapper categoryMapper;
    @Resource
    private TagMapper tagMapper;
    @Resource
    private HttpServletRequest request;
    @Resource
    private RedisUtil redisUtil;
    /**
     * 访客
     */
    public static final String UNIQUE_VISITOR = "unique_visitor";
    /**
     * 博客浏览量
     */
    public static final String BLOG_VIEWS_COUNT = "blog_views_count";

    @Override
    public BlogInfoDTO getBlogInfo() {
        // 查询文章数量
        long articleCount = articleMapper.selectCount(new LambdaQueryWrapper<Article>()
                .eq(Article::getStatus, 1));
        // 查询分类数量
        long categoryCount = categoryMapper.selectCount(null);
        // 查询标签数量
        long tagCount = tagMapper.selectCount(null);
        // 查询网站配置
        BlogInfo blogInfo = baseMapper.selectById(1);
        // 封装数据
        return BlogInfoDTO.builder()
                .articleCount(articleCount)
                .categoryCount(categoryCount)
                .tagCount(tagCount)
                .viewsCount(blogInfo.getVisitCount())
                .blogInfo(blogInfo)
                .build();
    }

    @Override
    public void report() {
        // 获取ip
        UserAgent userAgent = UserAgentUtil.parse(request.getHeader("user-agent"));
        String ipAddress = ServletUtil.getClientIP(request);
        // 获取访问设备
        Browser browser = userAgent.getBrowser();
        String name= userAgent.getPlatform().getName();
        // 生成唯一用户标识
        String uuid =  browser.getName() + name;
        String md5 = DigestUtils.md5DigestAsHex(uuid.getBytes());
        // 判断是否访问
        if (!redisUtil.isMember(UNIQUE_VISITOR, md5)) {
            // 访问量+1
            redisUtil.incr(BLOG_VIEWS_COUNT, 1);
            // 保存唯一标识
            redisUtil.add(UNIQUE_VISITOR, md5);
            BlogInfo blogInfo = baseMapper.selectById(1);
            baseMapper.update(null,new LambdaUpdateWrapper<BlogInfo>()
                .set(BlogInfo::getVisitCount,blogInfo.getVisitCount()+1).eq(BlogInfo::getId,blogInfo.getId()));
        }
    }

}
