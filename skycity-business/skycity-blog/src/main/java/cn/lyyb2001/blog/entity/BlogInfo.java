package cn.lyyb2001.blog.entity;

import cn.lyyb2001.common.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author daiyb2
 * @Title: BlogInfo
 * @Description:
 * @date 2021/11/21 14:06
 **/
@Data
public class BlogInfo extends BaseEntity {
    @TableId
    private Integer id;
    private String blogName;
    private String blogAvatar;
    private Integer visitCount;
}

