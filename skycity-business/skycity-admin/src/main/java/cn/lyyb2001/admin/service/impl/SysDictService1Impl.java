package cn.lyyb2001.admin.service.impl;

import cn.lyyb2001.admin.mapper.SysDictMapper;
import cn.lyyb2001.admin.pojo.entity.SysDict;
import cn.lyyb2001.admin.service.ISysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SysDictService1Impl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

}
