package cn.lyyb2001.admin.service;

import cn.lyyb2001.admin.pojo.dto.UserAuthDTO;
import cn.lyyb2001.admin.pojo.entity.SysUser;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysUserService extends IService<SysUser> {
    /**
     * 用户分页列表
     * @param page
     * @param user
     * @return
     */
    IPage<SysUser> list(Page<SysUser> page, SysUser user);
    /**
     * 根据用户名获取认证用户信息，携带角色和密码
     *
     * @param username
     * @return
     */
    UserAuthDTO getByUsername(String username);

    /**
     * 根据手机号码获取用户信息
     */
    UserAuthDTO getByMobilePhone(String mobilePhone);

    /**
     * 修改用户
     *
     * @param user
     * @return
     */
    boolean updateUser(SysUser user);
}
