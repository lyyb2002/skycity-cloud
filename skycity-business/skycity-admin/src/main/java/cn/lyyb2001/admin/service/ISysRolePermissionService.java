package cn.lyyb2001.admin.service;

import cn.lyyb2001.admin.pojo.dto.RolePermissionDTO;
import cn.lyyb2001.admin.pojo.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface ISysRolePermissionService extends IService<SysRolePermission> {

    List<Long> listPermissionId(Long moduleId, Long roleId);
//    List<Long> listPermissionId(Long roleId);
    boolean update(RolePermissionDTO rolePermission);


}
