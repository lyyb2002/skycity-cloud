package cn.lyyb2001.admin.controller;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import cn.lyyb2001.admin.pojo.dto.RolePermissionDTO;
import cn.lyyb2001.admin.pojo.entity.SysRole;
import cn.lyyb2001.admin.service.ISysPermissionService;
import cn.lyyb2001.admin.service.ISysRoleMenuService;
import cn.lyyb2001.admin.service.ISysRolePermissionService;
import cn.lyyb2001.admin.service.ISysRoleService;
import cn.lyyb2001.common.constants.GlobalConstants;
import cn.lyyb2001.common.model.Result;
import cn.lyyb2001.common.utils.JwtUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//@Api(tags = "角色接口")
@RestController
@RequestMapping("/api/v1/roles")
@Slf4j
public class RoleController {
    @Resource
    private ISysRoleService iSysRoleService;
    @Resource
    private ISysRoleMenuService iSysRoleMenuService;
    @Resource
    private ISysRolePermissionService iSysRolePermissionService;
    @Resource
    private ISysPermissionService iSysPermissionService;


//    @ApiOperation(value = "列表分页")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", dataType = "Long"),
//            @ApiImplicitParam(name = "limit", value = "每页数量", paramType = "query", dataType = "Long"),
//            @ApiImplicitParam(name = "name", value = "角色名称", paramType = "query", dataType = "String"),
//    })
    @GetMapping("/page")
    public Result<Page<SysRole>> pageList(Page<SysRole> pageVo, String name) {
        LambdaQueryWrapper<SysRole> queryWrapper = new LambdaQueryWrapper<SysRole>()
                .like(StrUtil.isNotBlank(name), SysRole::getName, name)
                .orderByAsc(SysRole::getSort)
                .orderByDesc(SysRole::getUpdateTime)
                .orderByDesc(SysRole::getCreateTime);
        Page<SysRole> result = iSysRoleService.page(pageVo, queryWrapper);
        return Result.success(result);
    }


    /**
     * 获取当前用户的角色
     * @return
     */
//    @ApiOperation(value = "角色列表")
    @GetMapping
    public Result<List<SysRole>> list() {
        List<String> roles = JwtUtils.getRoles();
        boolean isRoot = roles.contains(GlobalConstants.ROOT_ROLE_CODE);  // 判断是否是超级管理员
        List<SysRole> list = iSysRoleService.list(new LambdaQueryWrapper<SysRole>()
                .eq(SysRole::getStatus, GlobalConstants.STATUS_YES)
                .ne(!isRoot, SysRole::getCode, GlobalConstants.ROOT_ROLE_CODE)
                .orderByAsc(SysRole::getSort)
        );
        return Result.success(list);
    }


//    @ApiOperation(value = "新增角色")
//    @ApiImplicitParam(name = "role", value = "实体JSON对象", required = true, paramType = "body", dataType = "SysRole")
    @PostMapping
    public Result add(@RequestBody SysRole role) {
        long count = iSysRoleService.count(new LambdaQueryWrapper<SysRole>()
                .eq(SysRole::getCode, role.getCode())
                .or()
                .eq(SysRole::getName, role.getName())
        );
        Assert.isTrue(count == 0, "角色名称或角色编码重复，请检查！");
        boolean result = iSysRoleService.save(role);
        if (result) {
            iSysPermissionService.refreshPermRolesRules();
        }
        return Result.judge(result);
    }

//    @ApiOperation(value = "修改角色")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "角色id", required = true, paramType = "path", dataType = "Long"),
//            @ApiImplicitParam(name = "role", value = "实体JSON对象", required = true, paramType = "body", dataType = "SysRole")
//    })
    @PutMapping(value = "/{id}")
    public Result update(
            @PathVariable Long id,
            @RequestBody SysRole role) {
        long count = iSysRoleService.count(new LambdaQueryWrapper<SysRole>()
                .and(wq->wq.eq(SysRole::getCode, role.getCode())
                    .or().eq(SysRole::getName, role.getName()))
                .ne(SysRole::getId, id)
        );
        Assert.isTrue(count == 0, "角色名称或角色编码重复，请检查！");
        boolean result = iSysRoleService.updateById(role);
        if (result) {
            iSysPermissionService.refreshPermRolesRules();
        }
        return Result.judge(result);
    }

//    @ApiOperation(value = "删除角色")
//    @ApiImplicitParam(name = "ids", value = "以,分割拼接字符串", required = true, dataType = "String")
    @DeleteMapping("/{ids}")
    public Result delete(@PathVariable String ids) {
        boolean result = iSysRoleService.delete(Arrays.stream(ids.split(","))
                .map(Long::parseLong).collect(Collectors.toList()));
        if (result) {
            iSysPermissionService.refreshPermRolesRules();
        }
        return Result.judge(result);
    }

//    @ApiOperation(value = "选择性修改角色")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "用户ID", required = true, paramType = "path", dataType = "Long"),
//            @ApiImplicitParam(name = "role", value = "实体JSON对象", required = true, paramType = "body", dataType = "SysRole")
//    })
//    @PatchMapping(value = "/{id}")
//    public Result patch(@PathVariable Long id, @RequestBody SysRole role) {
//        LambdaUpdateWrapper<SysRole> updateWrapper = new LambdaUpdateWrapper<SysRole>()
//                .eq(SysRole::getId, id)
//                .set(role.getStatus() != null, SysRole::getStatus, role.getStatus());
//        boolean result = iSysRoleService.update(updateWrapper);
//        if (result) {
//            iSysPermissionService.refreshPermRolesRules();
//        }
//        return Result.judge(result);
//    }
//
//    @ApiOperation(value = "获取角色拥有的菜单ID集合")
//    @ApiImplicitParam(name = "id", value = "角色id", required = true, paramType = "path", dataType = "Long")
    @GetMapping("/{id}/menus")
    public Result listRoleMenu(@PathVariable("id") Long roleId) {
        List<Long> menuIds = iSysRoleMenuService.listMenuIds(roleId);
        return Result.success(menuIds);
    }

//    @ApiOperation(value = "获取角色拥有的权限ID集合")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "角色id", required = true, paramType = "path", dataType = "Integer"),
//            @ApiImplicitParam(name = "menuId", value = "菜单ID", paramType = "query", dataType = "Integer"),
//    })
    @GetMapping("/{id}/permissions")
    public Result listRolePermission(@PathVariable("id") Long roleId, Long menuId) {
        List<Long> permissionIds = iSysRolePermissionService.listPermissionId(menuId, roleId);
        return Result.success(permissionIds);
    }
//
//    @ApiOperation(value = "修改角色菜单")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "角色id", required = true, paramType = "path", dataType = "Long"),
//            @ApiImplicitParam(name = "role", value = "实体JSON对象", required = true, paramType = "body", dataType = "SysRole")
//    })
    @PutMapping(value = "/{id}/menus")
    @CacheEvict(cacheNames = "system",key = "'routeList'")
    public Result updateRoleMenu(
            @PathVariable("id") Long roleId,
            @RequestBody SysRole role) {
        List<Long> menuIds = role.getMenuIds();
        boolean result = iSysRoleMenuService.update(roleId, menuIds);
        if (result) {
            iSysPermissionService.refreshPermRolesRules();
        }
        return Result.judge(result);
    }

//    @ApiOperation(value = "修改角色权限")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id", value = "角色id", required = true, paramType = "path", dataType = "Long"),
//            @ApiImplicitParam(name = "rolePermission", value = "实体JSON对象", required = true, paramType = "body", dataType = "RolePermissionDTO")
//    })
    @PutMapping(value = "/{id}/permissions")
    public Result updateRolePermission(
            @PathVariable("id") Long roleId,
            @RequestBody RolePermissionDTO rolePermission) {
        rolePermission.setRoleId(roleId);
        boolean result = iSysRolePermissionService.update(rolePermission);
        if (result) {
            iSysPermissionService.refreshPermRolesRules();
        }
        return Result.judge(result);
    }
}
