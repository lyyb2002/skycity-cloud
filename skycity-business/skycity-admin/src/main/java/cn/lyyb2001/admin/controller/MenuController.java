package cn.lyyb2001.admin.controller;

import cn.lyyb2001.admin.pojo.entity.SysMenu;
import cn.lyyb2001.admin.pojo.vo.MenuVO;
import cn.lyyb2001.admin.pojo.vo.RouteVO;
import cn.lyyb2001.admin.pojo.vo.SelectVO;
import cn.lyyb2001.admin.pojo.vo.TreeSelectVO;
import cn.lyyb2001.admin.service.ISysMenuService;
import cn.lyyb2001.common.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 菜单控制器
 *
 * @author <a href="mailto:xianrui0365@163.com">xianrui</a>
 * @date 2020-11-06
 */
@RestController
@RequestMapping("/api/v1/menus")
@Slf4j
public class MenuController {
    @Resource
    private ISysMenuService menuService;
//    private final ISysPermissionService permissionService;

//    @ApiOperation(value = "菜单表格（Table）层级列表")
//    @ApiImplicitParam(name = "name", value = "菜单名称", paramType = "query", dataType = "String")
    @GetMapping("/table")
    public Result getTableList(String name) {
        List<MenuVO> menuList = menuService.listTable(name);
        return Result.success(menuList);
    }

//    @ApiOperation(value = "菜单下拉（Select）层级列表")
    @GetMapping("/select")
    public Result<List<SelectVO>> getSelectList() {
        List<SelectVO> menuList = menuService.listSelect();
        return Result.success(menuList);
    }

//    @ApiOperation(value = "菜单下拉（TreeSelect）层级列表")
    @GetMapping("/tree-select")
    public Result getTreeSelectList() {
        List<TreeSelectVO> menuList = menuService.listTreeSelect();
        return Result.success(menuList);
    }

    @GetMapping("/route")
    public Result getRouteList() {
        List<RouteVO> routeList = menuService.listRoute();
        return Result.success(routeList);
    }

//
//    @ApiOperation(value = "菜单详情")
//    @ApiImplicitParam(name = "id", value = "菜单id", required = true, paramType = "path", dataType = "Long")
//    @GetMapping("/{id}")
//    public Result detail(@PathVariable Integer id) {
//        SysMenu menu = menuService.getById(id);
//        return Result.success(menu);
//    }
//
//    @ApiOperation(value = "新增菜单")
    @PostMapping
//    @CacheEvict(cacheNames = "system",key = "'routeList'")
    public Result add(@RequestBody SysMenu menu) {
        boolean result = menuService.saveMenu(menu);
        return Result.judge(result);
    }
//
//    @ApiOperation(value = "修改菜单")
    @PutMapping(value = "/{id}")
//    @CacheEvict(cacheNames = "system",key = "'routeList'")
    public Result<String> update( @PathVariable Long id,@RequestBody SysMenu menu) {
        boolean result = menuService.updateMenu(menu);
        return Result.judge(result);
    }

//    @ApiOperation(value = "删除菜单")
//    @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
//    @DeleteMapping("/{ids}")
//    @CacheEvict(cacheNames = "system",key = "'routeList'")
//    public Result delete(@PathVariable("ids") String ids) {
//        boolean result = menuService.removeByIds(Arrays.asList(ids.split(",")));
//        if(result){
//            permissionService.refreshPermRolesRules();
//        }
//        return Result.judge(result);
//    }
//
//    @ApiOperation(value = "选择性修改菜单")
//    @PatchMapping(value = "/{id}")
//    @CacheEvict(cacheNames = "system",key = "'routeList'")
//    public Result patch(@PathVariable Integer id, @RequestBody SysMenu menu) {
//        LambdaUpdateWrapper<SysMenu> updateWrapper = new LambdaUpdateWrapper<SysMenu>().eq(SysMenu::getId, id);
//        updateWrapper.set(menu.getVisible() != null, SysMenu::getVisible, menu.getVisible());
//        boolean result = menuService.update(updateWrapper);
//        if(result){
//            permissionService.refreshPermRolesRules();
//        }
//        return Result.judge(result);
//    }
}
