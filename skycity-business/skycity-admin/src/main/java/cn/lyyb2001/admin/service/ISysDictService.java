package cn.lyyb2001.admin.service;


import cn.lyyb2001.admin.pojo.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;


public interface ISysDictService extends IService<SysDict> {
}
