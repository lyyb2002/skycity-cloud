package cn.lyyb2001.admin.service;

import cn.lyyb2001.admin.pojo.dto.RegisteredClientDTO;
import cn.lyyb2001.admin.pojo.entity.SysOauthClient;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysOauthClientService extends IService<SysOauthClient> {

    RegisteredClientDTO getById(String clientId);
}
