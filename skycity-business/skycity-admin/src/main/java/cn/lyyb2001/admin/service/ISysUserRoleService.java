package cn.lyyb2001.admin.service;


import cn.lyyb2001.admin.pojo.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysUserRoleService extends IService<SysUserRole> {
}
