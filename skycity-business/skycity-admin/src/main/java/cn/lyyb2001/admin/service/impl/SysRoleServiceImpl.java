package cn.lyyb2001.admin.service.impl;

import cn.hutool.core.lang.Assert;
import cn.lyyb2001.admin.mapper.SysRoleMapper;
import cn.lyyb2001.admin.pojo.entity.SysRole;
import cn.lyyb2001.admin.pojo.entity.SysRoleMenu;
import cn.lyyb2001.admin.pojo.entity.SysUserRole;
import cn.lyyb2001.admin.service.ISysRoleMenuService;
import cn.lyyb2001.admin.service.ISysRoleService;
import cn.lyyb2001.admin.service.ISysUserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    private ISysUserRoleService iSysUserRoleService;
    @Resource
    private ISysRoleMenuService iSysRoleMenuService;
//    @Resource
//    private ISysRoleP
//    private ISysRoleMenuService iSysRoleMenuService;
//    private ISysRolePermissionService iSysRolePermissionService;
//
//
    @Override
    public boolean delete(List<Long> ids) {
        Optional.ofNullable(ids).orElse(new ArrayList<>()).forEach(id -> {
            long count = iSysUserRoleService.count(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getRoleId, id));
            Assert.isTrue(count <= 0, "该角色已分配用户，无法删除");
            iSysRoleMenuService.remove(new LambdaQueryWrapper<SysRoleMenu>().eq(SysRoleMenu::getRoleId, id));
//            iSysRolePermissionService.remove(new LambdaQueryWrapper<SysRolePermission>().eq(SysRolePermission::getRoleId, id));
        });
        return this.removeByIds(ids);
    }

}
