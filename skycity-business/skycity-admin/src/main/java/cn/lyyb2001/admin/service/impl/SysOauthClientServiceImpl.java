package cn.lyyb2001.admin.service.impl;

import cn.hutool.json.JSONUtil;
import cn.lyyb2001.admin.mapper.SysOauthClientMapper;
import cn.lyyb2001.admin.pojo.dto.RegisteredClientDTO;
import cn.lyyb2001.admin.pojo.entity.SysOauthClient;
import cn.lyyb2001.admin.service.ISysOauthClientService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * @author daiyb2
 * @Title: SysOauthClientServiceImpl
 * @Description:
 * @date 2021/10/30 20:59
 **/
@Service
public class SysOauthClientServiceImpl extends ServiceImpl<SysOauthClientMapper, SysOauthClient> implements ISysOauthClientService {

    @Override
//    @Cacheable(value = "BasicDataCache",keyGenerator = "wiselyKeyGenerator")
    public RegisteredClientDTO getById(String clientId) {
//        SysOauthClient sysOauthClient =  baseMapper.selectById(clientId);

        SysOauthClient sysOauthClient = new SysOauthClient();
        sysOauthClient.setId("1572187310589296640");
        sysOauthClient.setClientId("zxg");
        sysOauthClient.setClientSecret("{noop}123456");
        sysOauthClient.setRedirectUris("http://www.baidu.com");
//         .redirectUri("http://127.0.0.1:9100/login/oauth2/code/mobile-gateway-client-oidc")
//                .redirectUri("http://127.0.0.1:9100/authorized")
        sysOauthClient.setScopes("message.read,email,phone");
        sysOauthClient.setClientAuthenticationMethods("client_secret_basic,client_secret_post");
        sysOauthClient.setAuthorizationGrantTypes("refresh_token,password,client_credentials,authorization_code");
        sysOauthClient.setClientSettings("{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":true}");
        sysOauthClient.setTokenSettings("{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",3600.000000000],\"settings.token.access-token-format\":{\"@class\":\"org.springframework.security.oauth2.core.OAuth2TokenFormat\",\"value\":\"self-contained\"},\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",7200.000000000]}");

        RegisteredClientDTO registeredClientDTO = new RegisteredClientDTO();
        registeredClientDTO.setId(sysOauthClient.getId());
        registeredClientDTO.setClientId(sysOauthClient.getClientId());
        registeredClientDTO.setClientSecret(sysOauthClient.getClientSecret());
        registeredClientDTO.setClientName(sysOauthClient.getClientName());
        registeredClientDTO.setClientAuthenticationMethods(new HashSet<>(List.of(sysOauthClient.getClientAuthenticationMethods().split(","))));
        registeredClientDTO.setAuthorizationGrantTypes(new HashSet<>(List.of(sysOauthClient.getAuthorizationGrantTypes().split(","))));
        registeredClientDTO.setScopes(new HashSet<>(List.of(sysOauthClient.getScopes().split(","))));
        registeredClientDTO.setRedirectUris(new HashSet<>(List.of(sysOauthClient.getRedirectUris().split(","))));
        registeredClientDTO.setClientSettings(JSONUtil.toBean(sysOauthClient.getClientSettings(), Map.class));
        registeredClientDTO.setTokenSettings(JSONUtil.toBean(sysOauthClient.getTokenSettings(), Map.class));
        return registeredClientDTO;
    }
}
