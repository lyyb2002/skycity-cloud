package cn.lyyb2001.admin.service.impl;

import cn.lyyb2001.admin.mapper.SysUserRoleMapper;
import cn.lyyb2001.admin.pojo.entity.SysUserRole;
import cn.lyyb2001.admin.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
