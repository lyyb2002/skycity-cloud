package cn.lyyb2001.admin.mapper;

import cn.lyyb2001.admin.pojo.dto.UserAuthDTO;
import cn.lyyb2001.admin.pojo.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author daiyb2
 * @Title: SysUserMapper
 * @Description:
 * @date 2021/10/30 19:21
 **/
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> list(Page<SysUser> page, SysUser user);

    UserAuthDTO getUserInfo(@Param("username") String username,@Param("mobile") String mobile);
}
