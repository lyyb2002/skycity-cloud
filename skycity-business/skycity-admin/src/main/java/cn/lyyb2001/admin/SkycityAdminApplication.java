package cn.lyyb2001.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author daiyb2
 * @Title: SkycityAdminApplication
 * @Description:
 * @date 2021/10/30 0:26
 **/
@SpringBootApplication
@EnableCaching
@EnableDiscoveryClient
public class SkycityAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(SkycityAdminApplication.class,args);
    }
}
