package cn.lyyb2001.admin.controller;

import cn.lyyb2001.admin.pojo.entity.SysDept;
import cn.lyyb2001.admin.pojo.vo.DeptVO;
import cn.lyyb2001.admin.pojo.vo.TreeSelectVO;
import cn.lyyb2001.admin.service.ISysDeptService;
import cn.lyyb2001.common.model.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/v1/depts")
public class DeptController {

    @Resource
    private ISysDeptService deptService;

//    @ApiOperation(value = "部门表格（Table）层级列表")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "name", value = "部门名称", paramType = "query", dataType = "String"),
//            @ApiImplicitParam(name = "status", value = "部门状态", paramType = "query", dataType = "Long"),
//    })
    @GetMapping("/table")
    public Result<List<DeptVO>> getTableList(String name) {
        List<DeptVO> deptTableList = deptService.listTable(name);
        return Result.success(deptTableList);
    }

    @GetMapping("/select")
    public Result<List<TreeSelectVO>> getSelectList() {
        List<TreeSelectVO> deptSelectList = deptService.listTreeSelect();
        return Result.success(deptSelectList);
    }

//    @ApiOperation(value = "部门详情")
//    @ApiImplicitParam(name = "id", value = "部门id", required = true, paramType = "path", dataType = "Long")
    @GetMapping("/{id}")
    public Result<SysDept> detail(@PathVariable Long id) {
        SysDept sysDept = deptService.getById(id);
        return Result.success(sysDept);
    }

//    @ApiOperation(value = "新增部门")
    @PostMapping
    public Result add(@RequestBody SysDept dept) {
        Long id = deptService.saveDept(dept);
        return Result.success(id);
    }

//    @ApiOperation(value = "修改部门")
    @PutMapping(value = "/{id}")
    public Result update(@PathVariable Long id, @RequestBody SysDept dept) {
        dept.setId(id);
        Long deptId = deptService.saveDept(dept);
        return Result.success(deptId);
    }

//    @ApiOperation(value = "删除部门")
//    @ApiImplicitParam(name = "ids", value = "部门ID，多个以英文逗号,分割拼接", required = true, paramType = "query", dataType = "String")
    @DeleteMapping("/{ids}")
    public Result delete(@PathVariable("ids") String ids) {
        boolean status= deptService.deleteByIds(ids);
        return Result.judge(status);
    }

}
