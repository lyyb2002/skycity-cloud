package cn.lyyb2001.admin.mapper;

import cn.lyyb2001.admin.pojo.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
