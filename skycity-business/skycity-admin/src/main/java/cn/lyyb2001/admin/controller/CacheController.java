package cn.lyyb2001.admin.controller;

import cn.lyyb2001.common.model.Result;
import com.google.common.collect.Lists;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName CacheController
 * @Decription TODO
 * @Author YingBo.Dai
 * @Date 2021/11/9
 * @Project cloud
 * 下午7:21 YingBo 2021
 **/
@RestController
@RequestMapping("/api/v1/cache")
public class CacheController {
    @Resource
    private CacheManager cacheManager;

    @GetMapping("/names")
    public Result getCacheNames(){
        Collection<String> list = cacheManager.getCacheNames();
        return Result.success(list);
    }

    @DeleteMapping("/{names}")
    public Result voi(@PathVariable String names) {
        List<String> nameList = Arrays.asList(names.split(","));
        nameList.forEach(s-> Objects.requireNonNull(cacheManager.getCache(s)).clear());
        return Result.success();
    }

    @GetMapping("/name/{cacheName}")
    public Result getCacheNames(@PathVariable String cacheName){
        Cache cache = cacheManager.getCache(cacheName);
        return Result.success(Lists.newArrayList(""));
    }
}
