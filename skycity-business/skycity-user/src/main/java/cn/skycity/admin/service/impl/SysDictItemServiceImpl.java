package cn.skycity.admin.service.impl;

import cn.skycity.admin.mapper.SysDictItemMapper;
import cn.skycity.admin.pojo.entity.SysDictItem;
import cn.skycity.admin.service.ISysDictItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2024/3/23
 * @Version 1.0
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements ISysDictItemService {
}
