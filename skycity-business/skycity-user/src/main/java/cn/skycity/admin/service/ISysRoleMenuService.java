package cn.skycity.admin.service;


import cn.skycity.admin.pojo.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    List<Long> listMenuIds(Long roleId);

    /**
     * 修改角色菜单
     * @param roleId
     * @param menuIds
     * @return
     */
    boolean update(Long roleId, List<Long> menuIds);

    /**
     * 获取角色下的所有权限值
     * @param roleIds
     * @return
     */
    List<String> listBtnPerms(List<Long> roleIds);
}
