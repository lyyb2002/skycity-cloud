package cn.skycity.admin.mapper;

import cn.skycity.admin.pojo.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    List<Long> listMenuIds(Long roleId);

    /**
     * 列出授权角色的按钮权限值
     * @param roleIds
     * @return
     */
    List<String> listPerms(@Param("roleIds") List<Long> roleIds);
}
