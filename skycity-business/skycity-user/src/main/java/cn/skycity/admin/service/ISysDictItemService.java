package cn.skycity.admin.service;

import cn.skycity.admin.pojo.entity.SysDictItem;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysDictItemService extends IService<SysDictItem> {
}
