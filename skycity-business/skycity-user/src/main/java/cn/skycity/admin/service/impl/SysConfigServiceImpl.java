package cn.skycity.admin.service.impl;

import cn.skycity.admin.mapper.SysConfigMapper;
import cn.skycity.admin.pojo.entity.SysConfig;
import cn.skycity.admin.service.SysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {


}
