package cn.skycity.admin.controller;

import cn.hutool.core.util.StrUtil;
import cn.skycity.admin.pojo.entity.SysUser;
import cn.skycity.admin.service.ISysUserService;
import cn.skycity.common.model.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/users")
@Slf4j
public class UserController {

    @Resource
    private ISysUserService sysUserService;
//    @Resource
//    private PasswordEncoder passwordEncoder;

    @GetMapping("/page")
    public Result<IPage<SysUser>> page(Page<SysUser> page, SysUser sysUser) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<SysUser>()
                .eq(sysUser.getDeptId()!=null,SysUser::getDeptId, sysUser.getDeptId())
                .like(StrUtil.isNotBlank(sysUser.getUsername()), SysUser::getUsername, sysUser.getUsername())
                .orderByAsc(SysUser::getUsername);
        return Result.success(sysUserService.page(page, queryWrapper));
    }

    @RequestMapping("/username/{username}")
    public Result<SysUser> getUserByUsername(@PathVariable String username) {
        return Result.success(sysUserService.getByUsername(username));
    }
}
