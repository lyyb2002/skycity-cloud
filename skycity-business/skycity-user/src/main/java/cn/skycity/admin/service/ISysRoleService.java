package cn.skycity.admin.service;


import cn.skycity.admin.pojo.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysRoleService extends IService<SysRole> {

}
