package cn.skycity.admin.service.impl;

import cn.skycity.admin.mapper.SysDictItemMapper;
import cn.skycity.admin.mapper.SysDictMapper;
import cn.skycity.admin.pojo.entity.SysDict;
import cn.skycity.admin.pojo.entity.SysDictItem;
import cn.skycity.admin.service.ISysDictService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2024/3/23
 * @Version 1.0
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

    @Autowired
    private SysDictItemMapper sysDictItemMapper;

    @Override
    @Transactional
    public void deleteByCode(String code) {
        sysDictItemMapper.delete(new LambdaQueryWrapper<SysDictItem>().eq(SysDictItem::getDictCode,code));
        baseMapper.delete(new LambdaQueryWrapper<SysDict>().eq(SysDict::getCode,code));
    }
}
