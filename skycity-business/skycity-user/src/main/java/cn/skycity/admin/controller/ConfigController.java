package cn.skycity.admin.controller;

import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import cn.skycity.admin.pojo.entity.SysConfig;
import cn.skycity.admin.service.SysConfigService;
import cn.skycity.common.model.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2024/3/24
 * @Version 1.0
 */
@RestController
@RequestMapping("/config")
@Slf4j
public class ConfigController {

    @Resource
    private SysConfigService sysConfigService;
    @GetMapping("/page")
    public Result<Page<SysConfig>> pageList(Page<SysConfig> pageVo, SysConfig sysConfig) {
        LambdaQueryWrapper<SysConfig> queryWrapper = new LambdaQueryWrapper<SysConfig>()
                .like(StrUtil.isNotBlank(sysConfig.getConfigName()), SysConfig::getConfigName, sysConfig.getConfigName())
                .eq(ObjUtil.isNotNull(sysConfig.getStatus()),SysConfig::getStatus,sysConfig.getStatus())
                .orderByDesc(SysConfig::getUpdateTime)
                .orderByDesc(SysConfig::getCreateTime);
        Page<SysConfig> result = sysConfigService.page(pageVo, queryWrapper);
        return Result.success(result);
    }


    @PostMapping
    public Result<Void> add(@RequestBody SysConfig sysConfig) {
        boolean result = sysConfigService.save(sysConfig);
        return Result.judge(result);
    }

    @PutMapping
    public Result<Void> update(@RequestBody SysConfig sysConfig){
        boolean result = sysConfigService.updateById(sysConfig);
        return Result.judge(result);
    }

    @DeleteMapping("/{id}")
    public Result<Void> delete(@PathVariable("id")Long id){
        boolean result = sysConfigService.removeById(id);
        return Result.judge(result);
    }

    @PostMapping("/updateStatus")
    public Result<?> updateStatus(@RequestBody SysConfig sysConfig){
        sysConfigService.update(new LambdaUpdateWrapper<SysConfig>().set(SysConfig::getStatus,sysConfig.getStatus()).eq(SysConfig::getId,sysConfig.getId()));
        return Result.success();
    }

}
