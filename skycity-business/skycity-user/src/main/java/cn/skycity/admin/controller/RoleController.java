package cn.skycity.admin.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import cn.skycity.admin.pojo.dto.RoleDTO;
import cn.skycity.admin.pojo.entity.SysRole;
import cn.skycity.admin.service.ISysRoleMenuService;
import cn.skycity.admin.service.ISysRoleService;
import cn.skycity.common.model.Result;
import cn.skycity.common.service.utils.LangUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mysql.cj.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2023/11/5
 * @Version 1.0
 */

@RestController
@RequestMapping("/api/roles")
@Slf4j
public class RoleController {

    @Resource
    private ISysRoleService iSysRoleService;
    @Resource
    private ISysRoleMenuService sysRoleMenuService;

    @GetMapping("/page")
    public Result<Page<SysRole>> pageList(Page<SysRole> pageVo, SysRole sysRole) {
        LambdaQueryWrapper<SysRole> queryWrapper = new LambdaQueryWrapper<SysRole>()
                .like(StrUtil.isNotBlank(sysRole.getName()), SysRole::getName, sysRole.getName()).or().
                like(StrUtil.isNotBlank( sysRole.getName()), SysRole::getCode,  sysRole.getName())
                .eq(ObjUtil.isNotNull(sysRole.getStatus()),SysRole::getStatus,sysRole.getStatus())
                .orderByDesc(SysRole::getUpdateTime)
                .orderByDesc(SysRole::getCreateTime);
        Page<SysRole> result = iSysRoleService.page(pageVo, queryWrapper);
        return Result.success(result);
    }


    @PostMapping
    public Result<Void> add(@RequestBody RoleDTO roleDTO){
        SysRole sysRole = BeanUtil.toBean(roleDTO, SysRole.class);
        long count = iSysRoleService.count(new LambdaQueryWrapper<SysRole>()
                .eq(SysRole::getCode, sysRole.getCode())
                .or()
                .eq(SysRole::getName, sysRole.getName())
        );
        Assert.isTrue(count == 0, LangUtil.getMessage("error.rolecode.repeat"));
        boolean result = iSysRoleService.save(sysRole);
        if (result) {
            sysRoleMenuService.update(sysRole.getId(), roleDTO.getMenuIds());
        }
        return Result.judge(result);
    }

    /**
     * 获取角色下的所有权限
     * @param roleId
     * @return
     */
    @GetMapping("/{id}/menus")
    public Result<List<Long>> listRoleMenu(@PathVariable("id") Long roleId) {
        List<Long> menuIds = sysRoleMenuService.listMenuIds(roleId);
        return Result.success(menuIds);
    }

    @PutMapping("/{id}")
    @Transactional
    public Result<Void> update(@RequestBody RoleDTO roleDTO){
        SysRole sysRole = BeanUtil.toBean(roleDTO, SysRole.class);
        long count = iSysRoleService.count(new LambdaQueryWrapper<SysRole>()
                .and(wq->wq.eq(SysRole::getCode, sysRole.getCode())
                        .or().eq(SysRole::getName, sysRole.getName()))
                .ne(SysRole::getId, sysRole.getId())
        );
        Assert.isTrue(count == 0, LangUtil.getMessage("error.rolecode.repeat"));
        boolean result = iSysRoleService.updateById(sysRole);
        if (result) {
            sysRoleMenuService.update(sysRole.getId(), roleDTO.getMenuIds());
        }
        return Result.judge(result);
    }

    @DeleteMapping("/{id}")
    public Result<Void> delete(@PathVariable("id")String id){
        List<Integer> ids = StringUtils.split(id,",",true).stream().map(Integer::parseInt).collect(Collectors.toList());
        boolean result = iSysRoleService.removeByIds(ids);
        return Result.judge(result);
    }

    @PostMapping("/setRoleStatus")
    public Result<?> setRoleStatus(@RequestBody SysRole sysRole){
        iSysRoleService.update(new LambdaUpdateWrapper<SysRole>().set(SysRole::getStatus,sysRole.getStatus())
                .eq(SysRole::getId,sysRole.getId()));
        return Result.success();
    }

    /**
     * 获取所有角色列列
     * @return
     */
    @GetMapping("/getAllRoleList")
    public Result<List<SysRole>> getAllRoleList(){
        return Result.success(iSysRoleService.list());
    }

}
