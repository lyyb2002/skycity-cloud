package cn.skycity.admin.service;

import cn.skycity.admin.pojo.dto.RegisteredClientDTO;

public interface ISysOauthClientService{

    RegisteredClientDTO getById(String id);

    RegisteredClientDTO getByClientId(String clientId);
}
