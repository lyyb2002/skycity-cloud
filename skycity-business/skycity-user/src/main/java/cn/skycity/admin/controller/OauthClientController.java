package cn.skycity.admin.controller;

import cn.skycity.admin.pojo.dto.RegisteredClientDTO;
import cn.skycity.admin.service.ISysOauthClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/oauth-clients")
@Slf4j
public class OauthClientController {

    @Resource
    private ISysOauthClientService iSysOauthClientService;

    @GetMapping("/id/{id}")
    public RegisteredClientDTO getById(@PathVariable String id) {
        return iSysOauthClientService.getById(id);
    }

    @GetMapping("/{clientId}")
    public RegisteredClientDTO getByClientId(@PathVariable String clientId) {
        return iSysOauthClientService.getByClientId(clientId);
    }
}
