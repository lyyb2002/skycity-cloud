package cn.skycity.admin.service;

import cn.skycity.admin.pojo.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysDictService extends IService<SysDict> {

    void deleteByCode(String code);
}
