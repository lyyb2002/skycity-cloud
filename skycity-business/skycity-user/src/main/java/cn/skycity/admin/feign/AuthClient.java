package cn.skycity.admin.feign;

import cn.skycity.common.model.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2024/3/23
 * @Version 1.0
 */
@FeignClient(name = "skycity-security")
public interface AuthClient {

    @PostMapping(value = "/oauth2/token")
    Result<Map<String, Object>> getToken(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("grant_type") String grantType, @RequestHeader("Authorization") String headerValue);

//    @PostMapping(value = "/oauth2/token")
//    ResultData<Map<String, Object>> refreshToken(@RequestParam("refresh_token") String refreshToken, @RequestParam("grant_type") String grantType, @RequestHeader("Authorization") String headerValue);


    @PostMapping("/logout")
    Result logout();

}
