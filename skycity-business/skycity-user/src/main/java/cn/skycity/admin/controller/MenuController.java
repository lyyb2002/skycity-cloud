package cn.skycity.admin.controller;

import cn.skycity.admin.pojo.entity.SysMenu;
import cn.skycity.admin.service.ISysMenuService;
import cn.skycity.common.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * 菜单管理
 */
@RestController
@RequestMapping("/api/menus")
@Slf4j
public class MenuController {
    @Resource
    private ISysMenuService menuService;

//    private final ISysPermissionService permissionService;

//    @ApiOperation(value = "菜单表格（Table）层级列表")
//    @ApiImplicitParam(name = "name", value = "菜单名称", paramType = "query", dataType = "String")
//    @GetMapping("/table")
//    public Result getTableList(String name) {
//        return Result.success(menuService.listTable(name));
//    }

//    @ApiOperation(value = "菜单下拉（Select）层级列表")
//    @GetMapping("/select")
//    public Result<List<SelectVO>> getSelectList() {
//        List<SelectVO> menuList = menuService.listSelect();
//        return Result.success(menuList);
//    }

    /**
     * 菜单管理，表格数据
     * @return
     */
//    @ApiOperation(value = "菜单下拉（TreeSelect）层级列表")
    @GetMapping("/tree-select")
    public Result getTreeSelectList(SysMenu menu) {
        return Result.success(menuService.listTreeSelect(menu));
    }



//
//    @ApiOperation(value = "菜单详情")
//    @ApiImplicitParam(name = "id", value = "菜单id", required = true, paramType = "path", dataType = "Long")
//    @GetMapping("/{id}")
//    public Result detail(@PathVariable Integer id) {
//        SysMenu menu = menuService.getById(id);
//        return Result.success(menu);
//    }
//

    @PostMapping("")
    public Result<?> addMenu(@RequestBody SysMenu sysMenu){
        return Result.success(menuService.saveMenu(sysMenu));
    }
//    @ApiOperation(value = "新增菜单")
//    @PostMapping("/add")
////    @CacheEvict(cacheNames = "system",key = "'routeList'")
//    public Result add(@RequestBody RouteVO menu) {
//        SysMenu sysMenu = new SysMenu();
//        RouteVO.Meta meta = menu.getMeta();
//        sysMenu.setParentId(menu.getParentId());
//        sysMenu.setMenuType(menu.getMenuType());
//        if(sysMenu.getMenuType()==1) {
//            sysMenu.setName(menu.getName());
//            sysMenu.setComponent(menu.getMeta().getComponent());
//            sysMenu.setPath(menu.getPath());
//            sysMenu.setRedirect(menu.getRedirect());
//            sysMenu.setLink(menu.getLink());
//            sysMenu.setIcon(meta.getIcon());
//        }else{
//            sysMenu.setPerm(menu.getPerm());
//        }
//        sysMenu.setSort(menu.getSort());
//        sysMenu.setTitle(meta.getTitle());
//        boolean result = menuService.saveMenu(sysMenu);
//        return Result.judge(result);
//    }
//
//    @ApiOperation(value = "修改菜单")
    @PutMapping(value = "/{id}")
////    @CacheEvict(cacheNames = "system",key = "'routeList'")
    public Result<String> update( @PathVariable Long id,@RequestBody SysMenu sysMenu) {
//        SysMenu sysMenu = new SysMenu();
//        sysMenu.setParentId(menu.getParentId());
//        sysMenu.setId(menu.getId());
//        RouteVO.Meta meta = menu.getMeta();
//        if(menu.getMenuType()==1) {
//            sysMenu.setName(menu.getName());
//            sysMenu.setComponent(menu.getMeta().getComponent());
//            sysMenu.setPath(menu.getPath());
//            sysMenu.setRedirect(menu.getRedirect());
//            sysMenu.setLink(meta.isLink() ? menu.getLink() : "");
//            sysMenu.setIcon(meta.getIcon());
//            sysMenu.setAffix(meta.isAffix() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
//            sysMenu.setBigScreen(meta.isBigScreen() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
//            sysMenu.setStatus(meta.isVisible() ? GlobalConstants.STATUS_YES : GlobalConstants.STATUS_NO);
//        }else{
//            sysMenu.setPerm(menu.getPerm());
//        }
//        sysMenu.setSort(menu.getSort());
//        sysMenu.setTitle(meta.getTitle());
        boolean result = menuService.updateMenu(sysMenu);
        return Result.judge(result);
    }

//    @ApiOperation(value = "删除菜单")
//    @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    @DeleteMapping("/{ids}")
//    @CacheEvict(cacheNames = "system",key = "'routeList'")
    public Result delete(@PathVariable("ids") String ids) {
        boolean result = menuService.removeByIds(Arrays.asList(ids.split(",")));
//        if(result){
//            permissionService.refreshPermRolesRules();
//        }
        return Result.judge(result);
    }

//    @ApiOperation(value = "选择性修改菜单")
//    @PatchMapping(value = "/{id}")
//    @CacheEvict(cacheNames = "system",key = "'routeList'")
//    public Result patch(@PathVariable Integer id, @RequestBody SysMenu menu) {
//        LambdaUpdateWrapper<SysMenu> updateWrapper = new LambdaUpdateWrapper<SysMenu>().eq(SysMenu::getId, id);
//        updateWrapper.set(menu.getVisible() != null, SysMenu::getVisible, menu.getVisible());
//        boolean result = menuService.update(updateWrapper);
//        if(result){
//            permissionService.refreshPermRolesRules();
//        }
//        return Result.judge(result);
//    }
}
