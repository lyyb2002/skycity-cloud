package cn.skycity.admin.mapper;

import cn.skycity.admin.pojo.entity.SysOauthClient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author daiyb2
 * @Title: SysUserMapper
 * @Description:
 * @date 2021/10/30 19:21
 **/
@Mapper
public interface SysOauthClientMapper extends BaseMapper<SysOauthClient> {

}
