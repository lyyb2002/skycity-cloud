package cn.skycity.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author daiyb2
 * @Title: SkycityAdminApplication
 * @Description:
 * @date 2021/10/30 0:26
 **/
@SpringBootApplication(scanBasePackages = {"cn.skycity.common.service","cn.skycity.admin"})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"cn.skycity.admin.feign"})
public class SkycityUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(SkycityUserApplication.class, args);
    }
}
