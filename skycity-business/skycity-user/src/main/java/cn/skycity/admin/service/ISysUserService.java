package cn.skycity.admin.service;

import cn.skycity.admin.pojo.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysUserService extends IService<SysUser> {
    /**
     * 根据用户名获取认证用户信息，携带角色和密码
     *
     * @param username
     * @return
     */
    SysUser getByUsername(String username);
}
