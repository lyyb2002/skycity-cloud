package cn.skycity.admin.service;


import cn.skycity.admin.pojo.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysConfigService extends IService<SysConfig> {

}
