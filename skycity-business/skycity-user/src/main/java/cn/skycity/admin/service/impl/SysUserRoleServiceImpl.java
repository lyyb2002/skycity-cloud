package cn.skycity.admin.service.impl;

import cn.skycity.admin.mapper.SysUserRoleMapper;
import cn.skycity.admin.pojo.entity.SysUserRole;
import cn.skycity.admin.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
