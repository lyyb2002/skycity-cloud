package cn.skycity.admin.controller;

import cn.hutool.core.util.StrUtil;
import cn.skycity.admin.pojo.entity.SysDict;
import cn.skycity.admin.pojo.entity.SysDictItem;
import cn.skycity.admin.service.ISysDictItemService;
import cn.skycity.admin.service.ISysDictService;
import cn.skycity.common.model.Result;
import cn.skycity.common.service.utils.LangUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description
 * @Author YingBo.Dai
 * @Date 2024/3/23
 * @Version 1.0
 */
@RestController
@RequestMapping("/dict")
@Slf4j
public class DictController {

    @Resource
    private ISysDictService sysDictService;
    @Resource
    private ISysDictItemService sysDictItemService;

    @GetMapping("/page")
    public Result<Page<SysDict>> pageList(Page<SysDict> pageVo, String name) {
        LambdaQueryWrapper<SysDict> queryWrapper = new LambdaQueryWrapper<SysDict>()
                .like(StrUtil.isNotBlank(name), SysDict::getName, name).or().
                like(StrUtil.isNotBlank(name), SysDict::getCode, name)
                .orderByDesc(SysDict::getUpdateTime)
                .orderByDesc(SysDict::getCreateTime);
        Page<SysDict> result = sysDictService.page(pageVo, queryWrapper);
        return Result.success(result);
    }

    @PostMapping
    public Result<?> add(@RequestBody SysDict sysDict){
        long count = sysDictService.count(new LambdaQueryWrapper<SysDict>().eq(SysDict::getCode, sysDict.getCode()));
        if(count>0){
            return Result.failed(LangUtil.getMessage("dict.code.exist"));
        }
        sysDictService.save(sysDict);
        return Result.success();
    }

    @PutMapping
    public Result<?> update(@RequestBody SysDict sysDict){
        long count = sysDictService.count(new LambdaQueryWrapper<SysDict>().eq(SysDict::getCode, sysDict.getCode()).ne(SysDict::getId,sysDict.getId()));
        if(count>0){
            return Result.failed(LangUtil.getMessage("dict.code.exist"));
        }
        sysDictService.updateById(sysDict);
        return Result.success();
    }

    @DeleteMapping("/{code}")
    public Result<?> delete(@PathVariable("code")String code){
        sysDictService.deleteByCode(code);
        return Result.success();
    }

    @PostMapping("/changeStatus")
    public Result<?> changeStatus(@RequestBody SysDict sysDict){
        sysDictService.update(new LambdaUpdateWrapper<SysDict>().set(SysDict::getStatus,sysDict.getStatus()).eq(SysDict::getId,sysDict.getId()));
        return Result.success();
    }

    /**
     *根据code获取字典项
     * @param code
     * @return
     */
    @GetMapping("/{code}")
    public Result<List<SysDictItem>> getDictItemList(@PathVariable("code") String code){
        LambdaQueryWrapper<SysDictItem> queryWrapper = new LambdaQueryWrapper<SysDictItem>()
                .like(StrUtil.isNotBlank(code), SysDictItem::getDictCode, code)
                .orderByAsc(SysDictItem::getSort);
        return Result.success(sysDictItemService.list(queryWrapper));
    }


    @PostMapping("/dictItem")
    public Result<?> addDictItem(@RequestBody SysDictItem sysDictItem){
        long count = sysDictItemService.count(new LambdaQueryWrapper<SysDictItem>().eq(SysDictItem::getDictCode,sysDictItem.getDictCode()).eq(SysDictItem::getName,sysDictItem.getName()));
        if(count>0){
            return Result.failed(LangUtil.getMessage("dict.code.exist"));
        }
        sysDictItemService.save(sysDictItem);
        return Result.success();
    }

    @PutMapping("/dictItem")
    public Result<?> updateDictItem(@RequestBody SysDictItem sysDictItem){
        long count = sysDictItemService.count(new LambdaQueryWrapper<SysDictItem>().eq(SysDictItem::getDictCode,sysDictItem.getDictCode())
                .eq(SysDictItem::getName,sysDictItem.getName()).ne(SysDictItem::getId,sysDictItem.getId()));
        if(count>0){
            return Result.failed(LangUtil.getMessage("dict.code.exist"));
        }
        sysDictItemService.updateById(sysDictItem);
        return Result.success();
    }

    @DeleteMapping("/dictItem/{id}")
    public Result<?> deleteDictItem(@PathVariable("id")Long id){
        sysDictItemService.removeById(id);
        return Result.success();
    }

    @PostMapping("/changeDictStatus")
    public Result<?> changeDictStatus(@RequestBody SysDictItem sysDictItem){
        sysDictItemService.update(new LambdaUpdateWrapper<SysDictItem>().set(SysDictItem::getStatus,sysDictItem.getStatus()).eq(SysDictItem::getId,sysDictItem.getId()));
        return Result.success();
    }

}
