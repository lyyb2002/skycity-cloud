package cn.skycity.admin.service.impl;

import cn.skycity.admin.mapper.UserMapper;
import cn.skycity.admin.pojo.entity.SysUser;
import cn.skycity.admin.service.ISysUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author daiyb2
 * @Title: SysUserServiceImpl
 * @Description:
 * @date 2021/10/30 19:19
 **/
@Service
public class SysUserServiceImpl extends ServiceImpl<UserMapper,SysUser> implements ISysUserService {

    @Override
    public SysUser getByUsername(String username) {
        return baseMapper.selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, username).eq(SysUser::getDeleted, 0));
    }

}
