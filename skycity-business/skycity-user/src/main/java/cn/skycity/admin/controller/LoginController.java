package cn.skycity.admin.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.skycity.admin.feign.AuthClient;
import cn.skycity.admin.pojo.entity.SysRole;
import cn.skycity.admin.pojo.entity.SysUser;
import cn.skycity.admin.pojo.entity.SysUserRole;
import cn.skycity.admin.pojo.vo.RouteVO;
import cn.skycity.admin.pojo.vo.UserVO;
import cn.skycity.admin.service.*;
import cn.skycity.common.constants.SecurityConstants;
import cn.skycity.common.model.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class LoginController {
    @Autowired
    private AuthClient authClient;
    @Autowired
    private ISysMenuService menuService;
    @Autowired
    private ISysUserRoleService sysUserRoleService;
    @Autowired
    private ISysRoleService sysRoleService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysRoleMenuService sysRoleMenuService;

    @RequestMapping("/oauth2/code/{clientId}")
    public String code(@PathVariable("clientId")String clientId, @RequestParam("code")String code){
        return clientId+":"+code;
    }

    /**
     * 自定义登录
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/login")
    public Result<Map<String,Object>> login(String username,String password,
            String grant_type,@RequestHeader("Authorization")String authorization){
        return authClient.getToken(username,password,grant_type,authorization);
    }

    @PostMapping("/logout")
    public  Result<?> logout(){
        authClient.logout();
        return Result.success();
    }


    @GetMapping("/me")
    public Result<UserVO> getCurrentUser(HttpServletRequest request) {
        String userName = request.getHeader(SecurityConstants.HEADER_OAUTH_USER);
        UserVO userVO = new UserVO();
        // 用户基本信息
        SysUser user = sysUserService.getByUsername(userName);
        BeanUtil.copyProperties(user, userVO);
        List<Long> roleList = sysUserRoleService.list(new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getUserId,user.getId())).stream().map(SysUserRole::getRoleId).collect(Collectors.toList());

        List<String> roleNames = sysRoleService.list(new LambdaQueryWrapper<SysRole>().in(SysRole::getId,roleList))
                .stream().map(SysRole::getCode).collect(Collectors.toList());
        userVO.setRoles(roleNames);
        //用户按钮权限信息
        List<String> perms = sysRoleMenuService.listBtnPerms(roleList);
        userVO.setPerms(perms);
        return Result.success(userVO);
    }
    @GetMapping("/routes")
    public Result getRouteList() {
        List<RouteVO> routeList = menuService.listRoute();
        return Result.success(routeList);
    }
}
