package cn.skycity.admin.service;


import cn.skycity.admin.pojo.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ISysUserRoleService extends IService<SysUserRole> {
}
