package cn.skycity.gateway.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * 从spring.ignores节点下面获取要忽略的路
 */
@Data
@Component
@ConfigurationProperties("spring.ignore")
public class IgnoreProperties {

    private List<String> urls = new ArrayList<>();

    private static final AntPathMatcher matcher = new AntPathMatcher();

    /**
     * 检查路径是否匹配
     * @param path
     * @return
     */
    public boolean containsPath(String path) {
        if (urls == null || urls.size() == 0) {
            return false;
        }
        return urls.stream().anyMatch(url->matcher.match(url,path));
    }
}
