package cn.skycity.gateway.config;

import cn.skycity.common.model.ResultCode;
import cn.skycity.common.utils.ResponseUtils;
import cn.skycity.gateway.oauth2.LoginLoseHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.server.resource.introspection.ReactiveOpaqueTokenIntrospector;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authorization.ServerAccessDeniedHandler;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

/**
 * 资源服务器配置
 */
@EnableWebFluxSecurity
public class ReactiveWebSecurityConfiguration {
    @Resource
    private AuthorizationManager authManagerHandler;
    /// 登录信息失效处理器
    @Resource
    private LoginLoseHandler loginLoseHandler;
    @Resource
    private ReactiveOpaqueTokenIntrospector reactiveOpaqueTokenIntrospector;

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http.oauth2ResourceServer().opaqueToken().introspector(reactiveOpaqueTokenIntrospector)
            .and().and()
            .authorizeExchange().anyExchange().access(authManagerHandler)   // 鉴权管理器配置
            .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler()).authenticationEntryPoint(loginLoseHandler)
            .and().cors().disable().csrf().disable();
        http.headers().frameOptions().disable();
        return http.build();
    }

    @Bean
    ServerAccessDeniedHandler accessDeniedHandler() {
        return (exchange, denied) -> {
            Mono<Void> mono = Mono.defer(() -> Mono.just(exchange.getResponse()))
                    .flatMap(response -> ResponseUtils.writeErrorInfo(response, ResultCode.ACCESS_UNAUTHORIZED));
            return mono;
        };
    }

}