package cn.skycity.gateway.config;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.net.URI;

/**
 * 鉴权决策器接口实现具体鉴权功能
 * @Date: 2023-05-26 13:51
 */
@Component
@Slf4j
public class AuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {

    @Resource
    private IgnoreProperties ignoreProperties;

    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> authentication,
            AuthorizationContext object) {
        URI request = object.getExchange().getRequest().getURI();
        String requestUrl = request.getPath();
        if (ignoreProperties.containsPath(requestUrl)) {
            return Mono.just(new AuthorizationDecision(true));
        }
        log.debug(requestUrl);
        //认证通过且有设置角色的用户可访问当前路径
        return authentication.filter(Authentication::isAuthenticated)
                .flatMapIterable(Authentication::getAuthorities)
                .map(GrantedAuthority::getAuthority)
                .any(StrUtil::isNotBlank)
                .map(AuthorizationDecision::new)
                .defaultIfEmpty(new AuthorizationDecision(false));
    }

}