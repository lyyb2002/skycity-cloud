package cn.skycity.gateway.config;

import cn.hutool.core.util.StrUtil;
import cn.skycity.common.constants.SecurityConstants;
import cn.skycity.common.model.ResultCode;
import cn.skycity.common.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.net.URI;
import java.util.Date;

@Component
@Slf4j
public class GlobalSecurityGlobalFilter implements GlobalFilter, Ordered {
    @Resource
    private IgnoreProperties ignoreProperties;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 获取session，通过session中SPRING_SECURITY_CONTEXT获取认证上下文
        ServerHttpRequest request = exchange.getRequest();
        URI uri = request.getURI();
        if (ignoreProperties.containsPath(uri.getPath())) {
            return chain.filter(exchange);
        }
        log.debug("**********UASFilter start: " + new Date());
        try {
            String authentication = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
            if (StrUtil.isBlank(authentication)) {
                return unauthorized(exchange);
            }
            //检查redis中是否含有该accessCode，如果不存在，则表示该code已经注销了。

            String method = request.getMethodValue();
            String url = request.getPath().value();
            log.debug("url:{},method:{},headers:{}", url, method, authentication);
            ServerHttpRequest.Builder builder = request.mutate();
            //将token存放到gatewayToken中，后续所有的web请求，如果能从header中获取到该请求，表示请求是从gateway转发的。
            builder.header(SecurityConstants.GATEWAY_TOKEN, authentication);
            return ReactiveSecurityContextHolder.getContext().flatMap(context -> {
                builder.header(SecurityConstants.HEADER_OAUTH_USER, context.getAuthentication().getName());
                return chain.filter(exchange.mutate().request(builder.build()).build());
            });
        } finally {
            log.debug("**********UASFilter end: " + new Date());
        }
    }


    protected Mono<Void> unauthorized(ServerWebExchange serverWebExchange, String... msg) {
        return Mono.defer(() -> Mono.just(serverWebExchange.getResponse()))
                .flatMap(response -> ResponseUtils.writeErrorInfo(response, ResultCode.TOKEN_INVALID_OR_EXPIRED));
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
