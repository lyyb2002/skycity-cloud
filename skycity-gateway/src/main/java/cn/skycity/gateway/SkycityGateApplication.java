package cn.skycity.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author daiyb2
 * @Title: SkycityGateApplication
 * @Description:
 * @date 2021/10/27 9:22
 **/
@SpringBootApplication
public class SkycityGateApplication {
    public static void main(String[] args) {
        SpringApplication.run(SkycityGateApplication.class, args);
    }

}
