import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

public class MySQLGenerate {
    private final static String dbUrl = "jdbc:mysql://120.77.38.39:6606/mall_pms?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true";
    private final static String driver = "com.mysql.cj.jdbc.Driver";
    private final static String userName = "root";
    private final static String password = "abcd-1234";

    public static void main(String[] args) {
        FastAutoGenerator.create(new DataSourceConfig.Builder(dbUrl, userName, password))
                .globalConfig(builder -> {
                    builder.author("YingBo.Dai")
                            .outputDir("E:\\myworkspace\\skycity\\skycity-cloud\\skycity-business\\skycity-pms\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("cn.lyyb2001")
                            .moduleName("pms");
                })
                .strategyConfig(builder -> {
                    builder.addInclude("PMS_ATTRIBUTE").addTablePrefix("pms_").entityBuilder().enableLombok();
                })
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}
